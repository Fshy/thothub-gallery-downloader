#!/usr/bin/env node
const _ = require('lodash');
const fs = require('fs');
const axios = require('axios');
const chalk = require('chalk');
const cheerio = require('cheerio');
const puppeteer = require('puppeteer');

const [,, ...args] = process.argv;
const url = args[0];

function request(reqArr, ind, title) {
  if (ind === reqArr.length) return;
  const filename = reqArr[ind].slice(reqArr[ind].lastIndexOf('/') + 1, reqArr[ind].length);
  return axios({
    method: 'GET',
    url: reqArr[ind],
    responseType: 'stream',
  }).then((resp) => {
    console.log(`${chalk.cyanBright(`[${(ind + 1).toString().padStart(reqArr.length.toString().length, '0')}/${reqArr.length}]`)} ${filename}`);
    if (args[1] && args[1] === ('-d' || '--dir')) {
      resp.data.pipe(
        fs.createWriteStream(`${title}/${filename}`),
      );
    } else {
      resp.data.pipe(
        fs.createWriteStream(`${filename}`),
      );
    }
    if (ind >= reqArr.length) {
      return console.log(chalk.greenBright('Download Completed!'));
    }
    return request(reqArr, ind + 1, title);
  });
}

if (!url) {
  console.log(chalk.redBright('No URL Passed to Program'));
} else {
  (async () => {
    const browser = await puppeteer.launch({
      headless: true,
      slowMo: 100,
      devtools: true,
    });
    try {
      const page = await browser.newPage();
      await page.setViewport({ width: 1920, height: 1080 });
      await page.goto(url, { waitUntil: 'domcontentloaded' });
      await page.click('.mace-gallery-teaser-button');
      await page.waitFor(3000);
      await page.click('div .g1-gallery-thumbs-button');
      await page.waitFor(3000);
      const content = await page.content();
      const $ = cheerio.load(content);
      const reqArr = _.map($('.g1-gallery-thumbnail'), (i) => i.children[0].attribs.src.replace('-180x120', ''));
      await page.close();
      await browser.close();
      if (reqArr.length === 0) return;
      // Write to Disk
      const illegalChars = RegExp(/[\\/:"*?<>|]/g);
      const title = $('div .g1-gallery-title').text().replace(illegalChars, '');
      if (args[1] && args[1] === ('-d' || '--dir')) {
        if (!fs.existsSync(title)) {
          fs.mkdirSync(title);
        }
      }
      console.log(chalk.magentaBright(title));
      console.log(chalk.greenBright('Starting Download..'));
      return request(reqArr, 0, title);
    } catch (error) {
      console.log(error);
      await browser.close();
    }
  })();
}
